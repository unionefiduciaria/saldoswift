package it.unionefiduciaria.swift.saldi.SaldoSwift.application.port;

import com.prowidesoftware.swift.model.SwiftMessage;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface ISwiftMessageManager {

    int[][] businesseProcedure(String directoryInput, String directoryOutput,  String typeFileOutput) throws IOException;

    List<SaldoSwift> swiftMessagesToSaldoSwifts(List<SwiftMessage> swiftMessageList, String nameDir);

    List<SaldoSwift> FileParseToSaldoSwifts(Path source) throws IOException;

    List<SwiftMessage> readResource(Path source) throws IOException;

    List<SwiftMessage> stringFinParserToSwiftMessage(String stringFin) throws IOException;

    List<SaldoSwift> stringFinParseToSaldoSwifts(String string, String nameData)throws IOException;

    int writeFileReport(List<SaldoSwift> saldoSwiftList, String directory, String type) throws IOException;

    int storeFile(List<SwiftMessage> swift, String directoryOutput) throws IOException;

    int saveLog(List<SaldoSwift> saldoSwifts);

    int saveSqlServer(List<SaldoSwift> saldoSwifts);

}