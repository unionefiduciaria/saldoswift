package it.unionefiduciaria.swift.saldi.SaldoSwift.application.port;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;

import java.io.IOException;
import java.util.List;

public interface IRepositoryResourcePort {

    int saveDb(List<SaldoSwift> saldo);

    int factoryFile(List<SaldoSwift> saldoSwiftList, String directoryOutput, String type) throws IOException;


}
