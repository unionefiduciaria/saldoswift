package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface JpaRepository extends CrudRepository<SaldoSwift, Long> {

}
