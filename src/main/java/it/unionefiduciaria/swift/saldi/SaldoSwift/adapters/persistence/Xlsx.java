package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class Xlsx implements IFileWriter {


    @Override
    public int save(List<SaldoSwift> saldoSwifts, String directoryOutput) throws IOException {

        String[] columns = {"bic",
                "messaggio",
                "mittente",
                "nomeFlusso",
                "nRifTransazione",
                "depositoSwift",
                "rapporto",
                "depositario",
                "tipoDeposisto",
                "depositoSf3",
                "dtSaldo",
                "segno",
                "divisa",
                "saldo",
                "cambio",
                "ctvDivConto",
                "divConto"};


        Workbook workbook = new XSSFWorkbook();

        CreationHelper create = workbook.getCreationHelper();

//        creo schema
        Sheet sheet = workbook.createSheet("SaldoSwift");


//        font celle di intestazione
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

//        stile cella
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

//        creo riga
        Row headerRow = sheet.createRow(0);

//        creo celle sulla base delle colonne dichiarate
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }




//        stile per la data
//        CellStyle cellStyle = workbook.createCellStyle();
//        cellStyle.setDataFormat(create.createDataFormat().getFormat("dd-mm-yyyy"));



//        creo una riga di celle per ogni entità saldoswift
        int rowNum = 1;
        for(SaldoSwift saldoSwift: saldoSwifts){

            Row row =  sheet.createRow(rowNum++);

            row.createCell(0).setCellValue(saldoSwift.getBic());
            row.createCell(1).setCellValue(saldoSwift.getMittente());
            row.createCell(2).setCellValue(saldoSwift.getMessaggio());
            row.createCell(3).setCellValue(saldoSwift.getNomeFlusso());
            row.createCell(4).setCellValue(saldoSwift.getNRifTransazione());
            row.createCell(5).setCellValue(saldoSwift.getDepositoSwift());
            row.createCell(6).setCellValue(saldoSwift.getRapporto());
            row.createCell(7).setCellValue(saldoSwift.getDepositario());
            row.createCell(8).setCellValue(saldoSwift.getTipoDeposito());
            row.createCell(9).setCellValue(saldoSwift.getDepositoSf3());
            row.createCell(10).setCellValue(saldoSwift.getDtSaldo());
            row.createCell(11).setCellValue(saldoSwift.getSegno());
            row.createCell(12).setCellValue(saldoSwift.getDivisa());
            row.createCell(13).setCellValue(saldoSwift.getSaldo());
            row.createCell(14).setCellValue(saldoSwift.getCambio());
            row.createCell(15).setCellValue(saldoSwift.getCtvDivConto());

        }

        for(int i = 0; i < columns.length; i++){
            sheet.autoSizeColumn(i);
        }




        FileOutputStream write = new FileOutputStream( directoryOutput +"\\"+ saldoSwifts.get(0).getNomeFlusso() + "-report.xlsx");
        workbook.write(write);
        write.close();

        return 1;

    }



}
