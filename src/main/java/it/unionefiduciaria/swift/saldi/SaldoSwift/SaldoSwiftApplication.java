package it.unionefiduciaria.swift.saldi.SaldoSwift;

import it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence.Csv;
import it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence.IFactoryType;
import it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence.IFileWriter;
import it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence.Xlsx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.IOException;


@SpringBootApplication
public class SaldoSwiftApplication{




	private static final Logger logger = LoggerFactory.getLogger(SaldoSwiftApplication.class);



	public static void main(String[] args) {
//		logger.trace("This is a debug message");
//		logger.info("This is an info message");
//		logger.warn("This is a warn message");
//		logger.error("This is an error message");
		SpringApplication.run(SaldoSwiftApplication.class, args);
	}


	@Bean
	public WebMvcConfigurer webMvcConfigurer(){
		return new WebMvcConfigurer(){};
	}

	@Bean
	public IFactoryType getFactoryType() {
		return new IFactoryType() {
			@Override
			public IFileWriter typeSave(String type) throws IOException {

				if (type.equalsIgnoreCase("csv")) {
					return new Csv();
				}
				else if(type.equalsIgnoreCase("xlsx")){
					return new Xlsx();
				}
				


				return null ;
			}

		};

	}


//	@Override
//	public void run(String... args0) throws Exception {
//		if (args0.length > 0 && args0[0].equals("exitcode")) {
//			throw new ExitException();
//		}
//	}
//	static class ExitException extends RuntimeException implements ExitCodeGenerator {
//		private static final long serialVersionUID = 1L;
//
//		@Override
//		public int getExitCode() {
//			return 10;
//		}
//
//	}
}
