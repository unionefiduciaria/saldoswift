package it.unionefiduciaria.swift.saldi.SaldoSwift.application.port;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.ConvertAmount;

public interface IRestExchangePort {


    ConvertAmount restModel(String divConto, String saldo, String divisa, String date);

}
