package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.shellAdapter;


import com.prowidesoftware.swift.model.SwiftMessage;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.ISwiftMessageManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.web.client.HttpClientErrorException;

import java.io.*;
import java.util.List;
import java.util.Scanner;

@ShellComponent
public class ShellMode {



	@Autowired
	ISwiftMessageManager swiftMessageManager;





	@ShellMethod(value = "inserire messaggio Swift", key = "ins" )
	public void addSwiftMessage(
			@ShellOption(value = "-out") String directoryOutput,
			@ShellOption(value = "-ty", defaultValue = "none")String typeFileOutput
			) throws IOException {


		try  {

			Scanner scanner = new Scanner(new InputStreamReader(System.in));
			StringBuilder stringBuilder = new StringBuilder();
			String stringIn ;

			while (scanner.hasNextLine()){

				stringIn = scanner.nextLine();
				stringBuilder.append(stringIn +"\n");

				if(stringIn.isEmpty()){
					break;

				}
			}


			stringIn = stringBuilder.toString();

			List<SwiftMessage> swiftMessages = this.swiftMessageManager.stringFinParserToSwiftMessage(stringIn);
			List<SaldoSwift> saldoSwifts = this.swiftMessageManager.swiftMessagesToSaldoSwifts(swiftMessages,"input da console");


			this.swiftMessageManager.storeFile(swiftMessages,directoryOutput);
			this.swiftMessageManager.saveLog(saldoSwifts);
			this.swiftMessageManager.saveSqlServer(saldoSwifts);


			if(!typeFileOutput.equalsIgnoreCase("none")){
				this.swiftMessageManager.writeFileReport(saldoSwifts, directoryOutput, typeFileOutput);
			}


		}catch (Exception e){
			e.printStackTrace();
		}
	}



	@ShellMethod(value = "Inserire percorso", key = "insconfig")
	public void addDir(@ShellOption(value = "-in" ) String directoryInput,
			@ShellOption(value = "-out" ) String directoryOutput,
			@ShellOption(value = "-ty", defaultValue = "none")String typeFileOutput) throws IOException,  HttpClientErrorException {


		try {

			System.out.println(directoryInput+"\n");

			int[][] report = this.swiftMessageManager.businesseProcedure(directoryInput , directoryOutput, typeFileOutput);

			StringBuilder strbuilderResult =  new StringBuilder();
			int dbRecord = 0;
			
			
			strbuilderResult.append("scrittura di " + report[0].length + " reportreport  \n");
			strbuilderResult.append("file di " + report[3].length + " flussi scritti \n");
			strbuilderResult.append("log di " + report[1].length + " flussi eseguito \n");



			for (int is : report[2])
			{
				dbRecord = dbRecord + is;
			}

			strbuilderResult.append("scrittura di "+ dbRecord + " record per " + report[2].length + " flussi");

			System.out.println(strbuilderResult.toString());


		} catch (IOException e) {
			e.printStackTrace();
		}catch (HttpClientErrorException e){
			e.printStackTrace();
		}

	}

}
