package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence;

import com.opencsv.CSVWriter;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;


import java.io.*;
import java.util.List;



public  class Csv implements IFileWriter {

    @Override
    public int save(List<SaldoSwift> saldoSwifts, String directoryOutput) throws IOException {




        String[] columns = {"bic",
                "messaggio",
                "mittente",
                "nomeFlusso",
                "nRifTransazione",
                "depositoSwift",
                "rapporto",
                "depositario",
                "tipoDeposisto",
                "depositoSf3",
                "dtSaldo",
                "segno",
                "divisa",
                "saldo",
                "cambio",
                "ctvDivConto",
                "divConto"};

        // mapping strategy - faccio la mappatura delle colonne con le loro posizioni
//        ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
//        strategy.setType(User.class);
//        strategy.setColumnMapping(columns);


//        File csvFile = new File("test.csv");

        PrintWriter pw = new PrintWriter(directoryOutput +"\\"+ saldoSwifts.get(0).getNomeFlusso() +"report.csv");
        CSVWriter csvWriter = new CSVWriter(pw,
                CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);


        for(SaldoSwift saldoSwift: saldoSwifts) {

            csvWriter.writeNext(new String[]{
                    saldoSwift.getBic(),
                    saldoSwift.getMittente(),
                    saldoSwift.getNomeFlusso(),
                    saldoSwift.getNRifTransazione(),
                    saldoSwift.getDepositoSwift(),
                    saldoSwift.getRapporto(),
                    saldoSwift.getDepositario(),
                    saldoSwift.getTipoDeposito(),
                    saldoSwift.getDepositoSf3(),
                    saldoSwift.getDtSaldo(),
                    saldoSwift.getSegno(),
                    saldoSwift.getDivisa(),
                    saldoSwift.getSaldo(),
                    saldoSwift.getCambio(),
                    saldoSwift.getCtvDivConto()});
        }


        csvWriter.close();

        return 1;
    }
}
