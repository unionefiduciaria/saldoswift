package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.logsManager;

import it.unionefiduciaria.swift.saldi.SaldoSwift.SaldoSwiftApplication;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.ILogsResourcePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class LogsResource implements ILogsResourcePort {

    private static final Logger logger = LoggerFactory.getLogger(SaldoSwiftApplication.class);

    @Override
    public int addLog(SaldoSwift saldoSwift) {


        logger.trace(saldoSwift.getBic()
                + " " + saldoSwift.getMittente()
                + " " + saldoSwift.getMessaggio()
                + " " + saldoSwift.getNomeFlusso()
                + " " + saldoSwift.getNRifTransazione()
                + " " + saldoSwift.getDepositoSwift()
                + " " + saldoSwift.getRapporto()
                + " " + saldoSwift.getDepositario()
                + " " + saldoSwift.getTipoDeposito()
                + " " + saldoSwift.getDepositoSf3()
                + " " + saldoSwift.getDtSaldo()
                + " " + saldoSwift.getSegno()
                + " " + saldoSwift.getDivisa()
                + " " + saldoSwift.getSaldo()
                + " " + saldoSwift.getCambio()
                + " " + saldoSwift.getCtvDivConto());

            return 1;
    }

}
