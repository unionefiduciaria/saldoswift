package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.api;


import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;

import java.util.List;



public interface IApiWebUserInterface {



    public ResponseEntity<List<SaldoSwift>> getSaldoSwift_ParseFilter(String strSwiftmessage);

}
