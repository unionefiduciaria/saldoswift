package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence;

import java.io.IOException;

public interface IFactoryType {

    public IFileWriter typeSave(String type) throws IOException;
}
