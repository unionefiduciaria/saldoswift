package it.unionefiduciaria.swift.saldi.SaldoSwift.application.port;

import com.prowidesoftware.swift.model.SwiftMessage;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface ILocalResourcePort {


    String readLocalFile(Path source) throws IOException;

    int saveLocalSwiftMessage(List<SwiftMessage> listSwiftMessage, String directoryOutput) throws IOException;
}
