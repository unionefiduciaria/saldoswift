package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.api;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.ConvertAmount;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.ResponseError;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.IRestExchangePort;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RestController
//@RequestMapping
public class RestExchangeResource implements IRestExchangePort {

    @Autowired
    private RestTemplateBuilder builder;

    @Autowired
    private  IExchangeService exchangeService;



    @Override
    public ConvertAmount restModel(String divConto, String saldo, String divisa, String date) throws HttpClientErrorException {

        RestTemplate restTemplate = this.builder.errorHandler(new ErrorHandlerRestExchangeResource()).build();

        ConvertAmount response = exchangeService.restModel(divConto, saldo, divisa, date); /*restTemplate.getForObject( divConto, saldo, divisa, date);*/

        return response;
    }



}


