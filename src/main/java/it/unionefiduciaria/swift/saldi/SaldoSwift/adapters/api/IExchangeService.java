package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.api;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.ConvertAmount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;

public interface IExchangeService {

    ConvertAmount restModel(String divConto, String saldo, String divisa, String date) throws HttpClientErrorException;
}
