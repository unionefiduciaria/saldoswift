package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface IFileWriter {

    int save(List<SaldoSwift> saldoSwifts, String directoryOutput) throws IOException;

}
