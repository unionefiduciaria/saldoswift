package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.fileManager;

import com.prowidesoftware.swift.io.writer.SwiftWriter;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.utils.Lib;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.ILocalResourcePort;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;

//  classe utilizzata per gestire fonte dati da file
@Component
public class LocalResource implements ILocalResourcePort {




//    leggo il file
    @Override
    public String readLocalFile(Path source) throws IOException {

        return Lib.readFile(source.toString());

    }



//     salvo una lista di messaggi un nuovo file per ogni messaggio swift
    @Override
    public int saveLocalSwiftMessage(List<SwiftMessage> listSwiftMessage, String directoryOutput) throws IOException {

        int writeVerify = 0;

        for (SwiftMessage swiftMessage : listSwiftMessage) {

            new File(directoryOutput +"\\" + LocalDate.now() + "\\" + swiftMessage.getSender()).mkdirs();

            FileWriter fileWriter = new FileWriter(directoryOutput +"\\"+ LocalDate.now() + "\\" + swiftMessage.getSender() + "\\"

                    + LocalDate.now() + swiftMessage.getUUID() + ".txt");

            BufferedWriter out = new BufferedWriter(fileWriter);

            SwiftWriter.writeMessage(swiftMessage, out);
            out.close();

            writeVerify++;

        }

        return writeVerify;
    }

}
