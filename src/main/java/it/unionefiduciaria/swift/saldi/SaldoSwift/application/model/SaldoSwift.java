package it.unionefiduciaria.swift.saldi.SaldoSwift.application.model;


import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "saldi")
public class SaldoSwift {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) // .IDETITY se non funziona qualcosa
    private Long id;

    @Column(name = "bic")
    private String bic;
    @Column(name = "messaggio")
    private String messaggio;
    @Column
    private String mittente;
    @Column
    private String nomeFlusso;
    @Column
    private String nRifTransazione;
    @Column
    private String depositoSwift;
    @Column
    private String rapporto;
    @Column
    private String depositario;
    @Column
    private String tipoDeposito;
    @Column
    private String depositoSf3;
    @Column
    private String dtSaldo;             /*DATA della transazione*/
    @Column
    private String segno;
    @Column
    private String divisa;              /*divisa di saldo*/
    @Column
    private String saldo;
    @Column
    public String cambio;               /*tasso di cambio(1 o 0)*/
    @Column
    public String ctvDivConto;          /*saldo convertito*/
    @Column
    public String divConto;             /*divisa relativa alla residenza*/


}
