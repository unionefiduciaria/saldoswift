package it.unionefiduciaria.swift.saldi.SaldoSwift.application.service;

import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.xml.transform.Source;


@Component
public class SwiftMessageManagerServiceImpl implements ISwiftMessageManager {


	@Autowired
	private ILocalResourcePort localResourcePort;

	@Autowired
	private ITypeParser typeParser;

	@Autowired
	private ILogsResourcePort logsResourcePort;

	@Autowired
	private IRepositoryResourcePort repositoryResourcePort;




	@Override
	public int[][] businesseProcedure(String directoryInput, String directoryOutput, String typeFileOutput) throws IOException {
		//        eseguo le procedure di lettura e di salvataggio dei fllussi


		List<Path> sources = new ArrayList<>();
		Stream<Path> path = Files.walk(Paths.get(directoryInput.replace("\\","/")));
		path.forEach(s -> sources.add(s));
		sources.remove(0);

		/*TODO:ritorno risualtato operazioni codificato*/
		

		int[][] reportProcess = new int[sources.size()][4];
		

		for (Path suorce : sources) {

			List<SwiftMessage> swiftMessages = readResource(suorce);
			List<SaldoSwift> saldoSwifts = swiftMessagesToSaldoSwifts(swiftMessages, suorce.toFile().getName());


			reportProcess[sources.indexOf(suorce)][0] = writeFileReport(saldoSwifts, directoryOutput , typeFileOutput);
			reportProcess[sources.indexOf(suorce)][1] = saveLog(saldoSwifts);
			reportProcess[sources.indexOf(suorce)][2] = saveSqlServer(saldoSwifts);
			reportProcess[sources.indexOf(suorce)][3] = storeFile(swiftMessages, directoryOutput);
		}

		return  reportProcess;

	}

	@Override
	public List<SaldoSwift> FileParseToSaldoSwifts(Path source) throws IOException {
		//    leggo il ile e  restituisco una lista di SaldiSwift

		return swiftMessagesToSaldoSwifts(readResource(source), source.toFile().getName());
	}

	@Override
	public List<SwiftMessage> readResource(Path source) throws IOException {
		//    leggo il  file  restituisco una lista di swiftmessage

		List<SwiftMessage> swiftMessageList = stringFinParserToSwiftMessage(localResourcePort.readLocalFile(source));

		return swiftMessageList;

	}

	@Override
	public List<SaldoSwift> stringFinParseToSaldoSwifts(String stringFin, String nameStream ) throws IOException {

		return swiftMessagesToSaldoSwifts(stringFinParserToSwiftMessage(stringFin), nameStream);
	}

	@Override
	public List<SwiftMessage> stringFinParserToSwiftMessage(String stringFin) throws IOException {

		List<SwiftMessage> listSwiftMessage = new ArrayList<>();
		
		
		AbstractMT abstractMT = AbstractMT.parse(stringFin);
		listSwiftMessage.add(abstractMT.getSwiftMessage());
		
		
		for (int i = 0; i < listSwiftMessage.get(0).getUnparsedTextsSize(); i++) {
	
			listSwiftMessage.add(listSwiftMessage.get(0).unparsedTextGetAsMessage(i));
		
		}
		
		listSwiftMessage.get(0).setUnparsedTexts(null);
		return listSwiftMessage;
	}

	@Override
	public List<SaldoSwift> swiftMessagesToSaldoSwifts(List<SwiftMessage> swiftMessageList, String nameDir) {

		List<SaldoSwift> saldiSwift = new ArrayList<>();

		for (SwiftMessage swiftMessage : swiftMessageList) {

			saldiSwift.add(this.typeParser.type(swiftMessage, nameDir));

		}
		return saldiSwift;
	}





	@Override
	public int writeFileReport(List<SaldoSwift> saldoSwiftList, String directoryOutput, String type) throws IOException {

		int status = this.repositoryResourcePort.factoryFile(saldoSwiftList, directoryOutput, type);
		return  status;

	}

	@Override
	public int storeFile(List<SwiftMessage> swiftMessageList, String directoryOutput) throws IOException {

		int resuslt = this.localResourcePort.saveLocalSwiftMessage(swiftMessageList, directoryOutput);

		if(swiftMessageList.size() == resuslt){
			return 1;
		}
		return 0;
	}

	@Override
	public int saveLog(List<SaldoSwift> saldoSwifts)  {

		int  i = 0;

		for(SaldoSwift saldoSwift : saldoSwifts) {
			i += this.logsResourcePort.addLog(saldoSwift);

		}
		if(saldoSwifts.size() == i){
			return 1;
		}
		return  0;
	}

	@Override
	public  int saveSqlServer(List<SaldoSwift> saldoSwifts){

		return this.repositoryResourcePort.saveDb(saldoSwifts);
	}


}