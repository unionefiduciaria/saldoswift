package it.unionefiduciaria.swift.saldi.SaldoSwift.application.port;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;

public interface ILogsResourcePort {

    int addLog(SaldoSwift saldoSwift) ;

}
