package it.unionefiduciaria.swift.saldi.SaldoSwift.application.service;

import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.mt.mt9xx.MT940;
import com.prowidesoftware.swift.model.mt.mt9xx.MT941;
import com.prowidesoftware.swift.model.mt.mt9xx.MT950;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.ConvertAmount;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.IRestExchangePort;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.ITypeParser;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Arrays;

@Component
public class TypeParser  implements ITypeParser {

    @Autowired
    IRestExchangePort exchange;


    @Override
    public String[] getExchangeValueFromConvertAmount(String divConto, String saldo, String divisa, String date){

        String[] exchange =  {"0","0"};

        if (divisa.equals(divConto)) {

            exchange[1] = "1";
        }
        else {

           ConvertAmount convertedAmount = this.exchange.restModel(divConto, saldo, divisa, date);

            if ( convertedAmount.getCurrency() != null) {

                exchange[0] = convertedAmount.getAmount().toString();
                exchange[1] = convertedAmount.getExchangeRates().get(0).getRate().toString();
            }

        }
        return exchange;
    }



    @Override
    public SaldoSwift type(SwiftMessage swiftMessage, String nameDir) {


        SaldoSwift saldoSwift = new SaldoSwift();

        if (Filter.filter(swiftMessage).ifType("940").toSwiftMessage() != null ) /*(swiftMessage.getType().equals("940"))*/ {


            MT940 mt = new MT940(swiftMessage);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");


            // String bic = msg2.getBlock1().getBIC().getBic8();
            saldoSwift.setBic(swiftMessage.getBlock1().getBIC().getBic8());
            saldoSwift.setMittente(mt.getSender());
            saldoSwift.setMessaggio(mt.getMessageType());
            saldoSwift.setNomeFlusso(nameDir);
            saldoSwift.setNRifTransazione(mt.getField20().getValue());
            saldoSwift.setDepositoSwift(mt.getField25().getValue().replaceAll("/", ""));
            saldoSwift.setRapporto("N.D");
            saldoSwift.setDepositario("N.D");
            saldoSwift.setTipoDeposito("N.D");
            saldoSwift.setDepositoSf3("N.D");
            saldoSwift.setDtSaldo(sdf.format(mt.getField60F().getDateAsCalendar().getTime()));
            saldoSwift.setSegno(mt.getField60F().getDCMark());
            saldoSwift.setDivisa(mt.getField60F().getCurrency());
            saldoSwift.setSaldo(mt.getField60F().getAmount());

             String[] conv = getExchangeValueFromConvertAmount(
                    mt.getField60F().getCurrency(),
                    mt.getField60F().getAmount().replaceAll(",","."),
                     "USD",
                    /*sdf.format(mt.getField60F().getDateAsCalendar().getTime().toString())*/"2020-10-18");

            saldoSwift.setDivConto("EUR");
            saldoSwift.setCambio(conv[0]);
            saldoSwift.setCtvDivConto(conv[1]);




        } else if (Filter.filter(swiftMessage).ifType("941").notIfSender(Arrays.asList("BPPBCHGG", "CBLUCH22")).toSwiftMessage() != null ) /*(swiftMessage.getType().equals("941"))*/ {

            MT941 mt = new MT941(swiftMessage);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");


            saldoSwift.setBic(swiftMessage.getBlock1().getBIC().getBic8());
            saldoSwift.setMittente(mt.getSender());
            saldoSwift.setMessaggio(mt.getMessageType());
            saldoSwift.setNomeFlusso(nameDir);
            saldoSwift.setNRifTransazione(mt.getField20().getValue());
            saldoSwift.setDepositoSwift(mt.getField25().getValue().replaceAll("/", ""));
            saldoSwift.setRapporto("N.D");
            saldoSwift.setDepositario("N.D");
            saldoSwift.setTipoDeposito("N.D");
            saldoSwift.setDepositoSf3("N.D");
            saldoSwift.setDtSaldo(sdf.format(mt.getField62F().getDateAsCalendar().getTime()));
            saldoSwift.setSegno(mt.getField62F().getDCMark());
            saldoSwift.setDivisa(mt.getField62F().getCurrency());
            saldoSwift.setSaldo(mt.getField62F().getAmount());


            String[] conv = getExchangeValueFromConvertAmount(
                    mt.getField62F().getCurrency(),
                    mt.getField62F().getAmount().replaceAll(",","."),
                    "USD",
                    /*sdf.format(mt.getField60F().getDateAsCalendar().getTime().toString())*/"2020-10-18");

            saldoSwift.setDivConto("EUR");
            saldoSwift.setCambio(conv[0]);
            saldoSwift.setCtvDivConto(conv[1]);


        } else if (Filter.filter(swiftMessage).ifType("950").notIfSender(Arrays.asList("VONTCHZZ", "BLFLCHBB", "BLFLLI2X")).toSwiftMessage() != null )/*(swiftMessage.getType().equals("950"))*/ {

            MT950 mt = new MT950(swiftMessage);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

            saldoSwift.setBic(swiftMessage.getBlock1().getBIC().getBic8());
            saldoSwift.setMittente(mt.getSender());
            saldoSwift.setMessaggio(mt.getMessageType());
            saldoSwift.setNomeFlusso(nameDir);
            saldoSwift.setNRifTransazione(mt.getField20().getValue());
            saldoSwift.setDepositoSwift(mt.getField25().getValue().replaceAll("/", ""));
            saldoSwift.setRapporto("N.D");
            saldoSwift.setDepositario("N.D");
            saldoSwift.setTipoDeposito("N.D");
            saldoSwift.setDepositoSf3("N.D");
            saldoSwift.setDtSaldo(sdf.format(mt.getField62F().getDateAsCalendar().getTime()));
            saldoSwift.setSegno(mt.getField62F().getDCMark());
            saldoSwift.setDivisa(mt.getField62F().getCurrency());
            saldoSwift.setSaldo(mt.getField62F().getAmount());

            String[] conv = getExchangeValueFromConvertAmount(
                    mt.getField62F().getCurrency(),
                    mt.getField62F().getAmount().replaceAll(",","."),
                    "USD",
                    /*sdf.format(mt.getField60F().getDateAsCalendar().getTime().toString())*/"2020-10-18");

            saldoSwift.setDivConto("EUR");
            saldoSwift.setCambio(conv[0]);
            saldoSwift.setCtvDivConto(conv[1]);

        }


//            logger.trace
//        System.out.println(et.getBic()
//                + " " + et.getMittente()
//                + " " + et.getMessaggio()
//                + " " + et.getNomeFlusso()
//                + " " + et.getNRifTransazione()
//                + " " + et.getDepositoSwift()
//                + " " + et.getRapporto()
//                + " " + et.getDepositario()
//                + " " + et.getTipoDeposisto()
//                + " " + et.getDepositoSf3()
//                + " " + et.getDtSaldo()
//                + " " + et.getSegno()
//                + " " + et.getDivisa()
//                + " " + et.getSaldo()
//                + " " + et.getCambio()
//                + " " + et.getCtvDivConto());

        return saldoSwift;
//        }


    }
}
