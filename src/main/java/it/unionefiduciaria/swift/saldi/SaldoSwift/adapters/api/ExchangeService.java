package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.api;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.ConvertAmount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


@Service
public class ExchangeService implements IExchangeService {

//    classe che si ccupa di interrogare altri micro servizi

    private RestTemplate restTemplate;

    @Autowired
    public void ExchangeService(RestTemplateBuilder restTemplateBuilder) {

//    crea un contenitore per le risposte rest per l'eventualità che una codice errore possa
//    bloccare l'esecuzione'

        this.restTemplate = restTemplateBuilder
                .errorHandler(new ErrorHandlerRestExchangeResource())
                .build();
    }


    @Override
    public ConvertAmount restModel(String divConto, String saldo, String divisa, String date) throws HttpClientErrorException {

//        eseguo la richiesta al micro servizio esteno

        ConvertAmount ConvertAmount = this.restTemplate.getForObject(
                "http://localhost:8080/convert-to?amount=" + saldo + "&currency_from=" + divConto + "&currency_to=" + divisa + "&date=" + date,
                ConvertAmount.class);

        return ConvertAmount;
    }
}
