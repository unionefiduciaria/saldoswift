package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.IRepositoryResourcePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

import java.io.IOException;
import java.util.List;

@Component
public class PortRepositoryImpl implements IRepositoryResourcePort {


    @Autowired
    private JpaRepository jpaRepository;

    @Autowired
    private IFactoryType factory;



    @Override
    public int factoryFile(List<SaldoSwift> saldoSwifts, String directoryOutput, String type) throws IOException {

        return this.factory.typeSave(type).save(saldoSwifts, directoryOutput);
        
    }


    @Override
    public int saveDb(List<SaldoSwift> saldo) {

        Iterable<SaldoSwift> resuslt = jpaRepository.saveAll(saldo);
        
        if(resuslt.equals(saldo)) {
        	return saldo.size() ;
        }
        return 0;
    }


}
