package it.unionefiduciaria.swift.saldi.SaldoSwift.application.port;

import com.prowidesoftware.swift.model.SwiftMessage;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;

public interface ITypeParser {

    String[] getExchangeValueFromConvertAmount(String divConto, String saldo, String divisa, String date);

    SaldoSwift type(SwiftMessage swiftMessage, String nameDir);
}
