package it.unionefiduciaria.swift.saldi.SaldoSwift.application.service;

import com.prowidesoftware.swift.model.SwiftMessage;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.ConvertAmount;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.ExchangeRate;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.IRestExchangePort;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.ISwiftMessageManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith({SpringExtension.class})
class TypeParserTest {


    @InjectMocks
    @Spy
    private TypeParser typeParser;

    @MockBean
    private IRestExchangePort restExchangePort;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }




    @Test
    void getExchangeValueFromConvertAmount() {

        ConvertAmount convertAmount = new ConvertAmount().exchangeRates(Arrays.asList(
                new ExchangeRate().rate(BigDecimal.valueOf(3)).currency("USD").date(LocalDate.of( 2020, 02, 10)),
                new ExchangeRate().rate(BigDecimal.valueOf(4)).currency("CHF").date(LocalDate.of(2020, 02, 10)))).
                amount(BigDecimal.valueOf(200)).
                currency("CHF");

        when(restExchangePort.restModel("CHF", "200", "USD", "10-02-2020")).
                thenReturn(convertAmount);


        String[] exchangeTest = this.typeParser.getExchangeValueFromConvertAmount("CHF", "200", "USD", "10-02-2020");

//        testing how it position the answere in the array
        assertEquals(exchangeTest[0],"200");
        assertEquals(exchangeTest[1], "3");

//        testing testing how  response with same currency
        String[] exchangeFailValuteTest = this.typeParser.getExchangeValueFromConvertAmount("CHF", "200", "CHF", "10-02-2020");
        assertEquals(exchangeFailValuteTest[0], "0");
        assertEquals(exchangeFailValuteTest[1], "1");

    }


    @Test
    void type() throws IOException {

        ISwiftMessageManager swiftMessageManager = new SwiftMessageManagerServiceImpl();
        String[] s = {"0","1"};
        ConvertAmount convertAmount = new ConvertAmount().exchangeRates(Arrays.asList(
                new ExchangeRate().rate(BigDecimal.valueOf(3)).currency("USD").date(LocalDate.of( 2020, 02, 10)),
                new ExchangeRate().rate(BigDecimal.valueOf(4)).currency("CHF").date(LocalDate.of(2020, 02, 10)))).
                amount(BigDecimal.valueOf(200)).
                currency("CHF");
        List<SwiftMessage> swiftMessage = swiftMessageManager.stringFinParserToSwiftMessage(
                /*940*/
                "{1:F01UBSWCHZHX80A0000000000}{2:I941UBSWCHZHXI1IN}{3:{113:9411}{108:FT9B4SAGU3GXQW2A}}{4:\n" +
                        ":20:Z020336VK7848079\n" +
                        ":25:/02300000386289600000R\n" +
                        ":28:1\n" +
                        ":62F:C201130EUR2715295,57\n" +
                        ":64:C201130EUR2715295,57\n" +
                        ":86:?IR:0,\n" +
                        "?UBS:+AINT:EUR0,\n" +
                        "+OCDE:159\n" +
                        "+PARI:02300000386289\n" +
                        "+AEXR:EUR1,08291988+REXR:EUR1,08291988\n" +
                        "-}" +
                        /*941*/
                        "{1:F01UBSWCHZHX80A0000000000}{2:I941UBSWCHZHXI1IN}{3:{113:9411}{108:EL9B4SAGXFDJE7RA}}{4:\n" +
                        ":20:Z020339VK8147642\n" +
                        ":25:/02300000386289600000R\n" +
                        ":28:1\n" +
                        ":62F:C201203EUR2715489,97\n" +
                        ":64:C201203EUR2715489,97\n" +
                        ":86:?IR:0,\n" +
                        "?UBS:+AINT:EUR0,\n" +
                        "+OCDE:159\n" +
                        "+PARI:02300000386289\n" +
                        "+AEXR:EUR1,08284802+REXR:EUR1,08284802\n" +
                        "-}"
                /*TODO:950*/

        );

        Mockito.doReturn(s).when(typeParser).getExchangeValueFromConvertAmount(any(),any(),any(),any());


        SaldoSwift saldoSwift = this.typeParser.type(swiftMessage.get(0),"testString");

        assumingThat(saldoSwift.getMessaggio().equals("941"), () -> assertAll(
                () -> assertNotEquals(saldoSwift.getMittente(), "BPPBCHGG"),
                () -> assertNotEquals(saldoSwift.getMittente(), "CBLUCH22")));

        assumingThat(saldoSwift.getMessaggio().equals("950"), () -> assertAll(
                () -> assertNotEquals(saldoSwift.getMittente(), "VONTCHZZ"),
                () -> assertNotEquals(saldoSwift.getMittente(), "BLFLCHBB"),
                () -> assertNotEquals(saldoSwift.getMittente(), "BLFLLI2X")));

    }
}