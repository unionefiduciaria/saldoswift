package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith({SpringExtension.class})
@DataJpaTest
class JpaRepositoryTest {


    @Autowired
    private JpaRepository jpaRepository;


    @Test
    public void testSave(){


        SaldoSwift saldoSwift = new SaldoSwift();

        saldoSwift.setBic("UBSWCHZH");
        saldoSwift.setMittente("941");
        saldoSwift.setMessaggio("UBSWCHZHX80A");
        saldoSwift.setNomeFlusso("nameSource");
        saldoSwift.setNRifTransazione("Z020336VK7848079");
        saldoSwift.setDepositoSwift("02300000386289600000R");
        saldoSwift.setRapporto("N.D");
        saldoSwift.setDepositario("N.D");
        saldoSwift.setTipoDeposito("N.D");
        saldoSwift.setDepositoSf3("N.D");
        saldoSwift.setDtSaldo("2020/11/30");
        saldoSwift.setSegno("C");
        saldoSwift.setDivisa("EUR");
        saldoSwift.setSaldo("2715295,57");
        saldoSwift.setCambio("0");
        saldoSwift.setCtvDivConto("EUR");

        SaldoSwift saldoSwift1 = saldoSwift;



        List<SaldoSwift> saldoSwifts = new ArrayList<>();
        saldoSwifts.add(saldoSwift);
        saldoSwifts.add(saldoSwift1);

        Iterable<SaldoSwift> s = this.jpaRepository.saveAll(saldoSwifts);
        assertEquals(s, saldoSwifts);

    }


}