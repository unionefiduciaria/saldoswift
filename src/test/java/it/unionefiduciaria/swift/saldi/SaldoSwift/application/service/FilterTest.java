package it.unionefiduciaria.swift.saldi.SaldoSwift.application.service;

import com.prowidesoftware.swift.model.SwiftMessage;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.ISwiftMessageManager;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class FilterTest {

    @InjectMocks
    Filter filter;

    @Autowired
    ISwiftMessageManager swiftMessageManager;
    private Filter filterTest;

    @Test
    void filter() throws IOException {

        List<SwiftMessage> swiftMessage = this.swiftMessageManager.stringFinParserToSwiftMessage(
                "{1:F01UBSWCHZHX80A0000000000}{2:I941UBSWCHZHXI1IN}{3:{113:9411}{108:FT9B4SAGU3GXQW2A}}{4:\n" +
                        ":20:Z020336VK7848079\n" +
                        ":25:/02300000386289600000R\n" +
                        ":28:1\n" +
                        ":62F:C201130EUR2715295,57\n" +
                        ":64:C201130EUR2715295,57\n" +
                        ":86:?IR:0,\n" +
                        "?UBS:+AINT:EUR0,\n" +
                        "+OCDE:159\n" +
                        "+PARI:02300000386289\n" +
                        "+AEXR:EUR1,08291988+REXR:EUR1,08291988\n" +
                        "-}");


        assertEquals(Filter.filter(swiftMessage.get(0)), swiftMessage);
    }

    @Test
    void notIfSender() {
    }

    @Test
    void testNotIfSender() {
    }

    @Test
    void ifType() {
    }

    @Test
    void testIfType() {
    }

    @Test
    void toSwiftMessage() {
    }
}