package it.unionefiduciaria.swift.saldi.SaldoSwift.application.service;

import com.prowidesoftware.swift.model.SwiftMessage;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.ILocalResourcePort;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.ILogsResourcePort;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.IRepositoryResourcePort;
import it.unionefiduciaria.swift.saldi.SaldoSwift.application.port.ITypeParser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.*;


import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@ExtendWith({SpringExtension.class})
class SwiftMessageManagerServiceImplTest {


    @InjectMocks
    @Spy
    private SwiftMessageManagerServiceImpl swiftMessageManagerService;

    @MockBean
    private ITypeParser typeParser;

    @MockBean
    private ILocalResourcePort localResourcePort;

    @MockBean
    private IRepositoryResourcePort repositoryResourcePort;

    @MockBean
    private ILogsResourcePort logsResourcePort;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }





    @Test
    void businesseProcedure(@TempDir Path tempTestPath) throws IOException {

        Path pathDataFlow = tempTestPath.resolve("dataFlow.txt");
        String test = "{1:F01UBSWCHZHX80A0000000000}{2:I941UBSWCHZHXI1IN}{3:{113:9411}{108:FT9B4SAGU3GXQW2A}}{4:\n" +
                ":20:Z020336VK7848079\n" +
                ":25:/02300000386289600000R\n" +
                ":28:1\n" +
                ":62F:C201130EUR2715295,57\n" +
                ":64:C201130EUR2715295,57\n" +
                ":86:?IR:0,\n" +
                "?UBS:+AINT:EUR0,\n" +
                "+OCDE:159\n" +
                "+PARI:02300000386289\n" +
                "+AEXR:EUR1,08291988+REXR:EUR1,08291988\n" +
                "-}"+
                "{1:F01UBSWCHZHX80A0000000000}{2:I941UBSWCHZHXI1IN}{3:{113:9411}{108:EL9B4SAGXFDJE7RA}}{4:\n" +
                ":20:Z020339VK8147642\n" +
                ":25:/02300000386289600000R\n" +
                ":28:1\n" +
                ":62F:C201203EUR2715489,97\n" +
                ":64:C201203EUR2715489,97\n" +
                ":86:?IR:0,\n" +
                "?UBS:+AINT:EUR0,\n" +
                "+OCDE:159\n" +
                "+PARI:02300000386289\n" +
                "+AEXR:EUR1,08284802+REXR:EUR1,08284802\n" +
                "-}";

        FileWriter fileWriter = new FileWriter(pathDataFlow.toString());
        fileWriter.write(test);
        fileWriter.close();

//        Mockito.when(swiftMessageManagerService.writeFileReport(any(),anyString(),anyString())).thenReturn("Test string writeFileReport\n");
        Mockito.doReturn("Test string saveLog\n").when(swiftMessageManagerService).saveLog(any());
        Mockito.doReturn("Test string saveSqlServer\n").when(swiftMessageManagerService).saveSqlServer(any());
        Mockito.doReturn("Test string stroreFile\n").when(swiftMessageManagerService).storeFile(any(),anyString());

        int result[] = this.swiftMessageManagerService.businesseProcedure(pathDataFlow.toString(),
                pathDataFlow.toString(), "stringType");

//        assertEquals(result,);


    }

    @Test
    void fileParseToSaldoSwifts() {
    }

    @Test
    void readResource() {
    }

    @Test
    void stringFinParseToSaldoSwifts() {
    }

    @Test
    void stringFinParserToSwiftMessage() throws IOException {

        List<SwiftMessage> swiftMessage = this.swiftMessageManagerService.stringFinParserToSwiftMessage(
                "{1:F01UBSWCHZHX80A0000000000}{2:I941UBSWCHZHXI1IN}{3:{113:9411}{108:FT9B4SAGU3GXQW2A}}{4:\n" +
                        ":20:Z020336VK7848079\n" +
                        ":25:/02300000386289600000R\n" +
                        ":28:1\n" +
                        ":62F:C201130EUR2715295,57\n" +
                        ":64:C201130EUR2715295,57\n" +
                        ":86:?IR:0,\n" +
                        "?UBS:+AINT:EUR0,\n" +
                        "+OCDE:159\n" +
                        "+PARI:02300000386289\n" +
                        "+AEXR:EUR1,08291988+REXR:EUR1,08291988\n" +
                        "-}"+
                        "{1:F01UBSWCHZHX80A0000000000}{2:I941UBSWCHZHXI1IN}{3:{113:9411}{108:EL9B4SAGXFDJE7RA}}{4:\n" +
                        ":20:Z020339VK8147642\n" +
                        ":25:/02300000386289600000R\n" +
                        ":28:1\n" +
                        ":62F:C201203EUR2715489,97\n" +
                        ":64:C201203EUR2715489,97\n" +
                        ":86:?IR:0,\n" +
                        "?UBS:+AINT:EUR0,\n" +
                        "+OCDE:159\n" +
                        "+PARI:02300000386289\n" +
                        "+AEXR:EUR1,08284802+REXR:EUR1,08284802\n" +
                        "-}");



        assertEquals(swiftMessage.size(),2);

    }

    @Test
    void swiftMessagesToSaldoSwifts() throws IOException {

        SaldoSwift saldoSwift = new SaldoSwift();

        saldoSwift.setBic("UBSWCHZH");
        saldoSwift.setMittente("941");
        saldoSwift.setMessaggio("UBSWCHZHX80A");
        saldoSwift.setNomeFlusso("nameSource");
        saldoSwift.setNRifTransazione("Z020336VK7848079");
        saldoSwift.setDepositoSwift("02300000386289600000R");
        saldoSwift.setRapporto("N.D");
        saldoSwift.setDepositario("N.D");
        saldoSwift.setTipoDeposito("N.D");
        saldoSwift.setDepositoSf3("N.D");
        saldoSwift.setDtSaldo("2020/11/30");
        saldoSwift.setSegno("C");
        saldoSwift.setDivisa("EUR");
        saldoSwift.setSaldo("2715295,57");
        saldoSwift.setCambio("0");
        saldoSwift.setCtvDivConto("EUR");

        List<SwiftMessage> swiftMessage = this.swiftMessageManagerService.stringFinParserToSwiftMessage(
                "{1:F01UBSWCHZHX80A0000000000}{2:I941UBSWCHZHXI1IN}{3:{113:9411}{108:FT9B4SAGU3GXQW2A}}{4:\n" +
                        ":20:Z020336VK7848079\n" +
                        ":25:/02300000386289600000R\n" +
                        ":28:1\n" +
                        ":62F:C201130EUR2715295,57\n" +
                        ":64:C201130EUR2715295,57\n" +
                        ":86:?IR:0,\n" +
                        "?UBS:+AINT:EUR0,\n" +
                        "+OCDE:159\n" +
                        "+PARI:02300000386289\n" +
                        "+AEXR:EUR1,08291988+REXR:EUR1,08291988\n" +
                        "-}");

        when( this.typeParser.type( swiftMessage.get(0),"nameSource")).thenReturn(saldoSwift);

        List<SaldoSwift>saldoSwiftTest = this.swiftMessageManagerService.swiftMessagesToSaldoSwifts(swiftMessage, "nameSource");

        assertThat(saldoSwift.equals(saldoSwiftTest.get(0)));


    }




    @Test
    void writeFileReport() throws IOException {
        /*TODO:refactor*/
//
//        when(repositoryResourcePort.factory(any(),anyString(),anyString())).thenReturn("testString");
//
//       String test = this.swiftMessageManagerService.writeFileReport(any(),anyString(),anyString());
//
//        assertEquals(test, "testString");

    }

    @Test
    void storeFile() throws IOException {

        when(this.localResourcePort.saveLocalSwiftMessage(any(),any())).thenReturn("stringTest");
        String responseTest = this.swiftMessageManagerService.storeFile(any(),any());

        assertEquals(responseTest, "stringTest");
    }

    @Test
    void saveLog() {

        SaldoSwift saldoSwift = new SaldoSwift();

        saldoSwift.setBic("UBSWCHZH");
        saldoSwift.setMittente("941");
        saldoSwift.setMessaggio("UBSWCHZHX80A");
        saldoSwift.setNomeFlusso("nameSource");
        saldoSwift.setNRifTransazione("Z020336VK7848079");
        saldoSwift.setDepositoSwift("02300000386289600000R");
        saldoSwift.setRapporto("N.D");
        saldoSwift.setDepositario("N.D");
        saldoSwift.setTipoDeposito("N.D");
        saldoSwift.setDepositoSf3("N.D");
        saldoSwift.setDtSaldo("2020/11/30");
        saldoSwift.setSegno("C");
        saldoSwift.setDivisa("EUR");
        saldoSwift.setSaldo("2715295,57");
        saldoSwift.setCambio("0");
        saldoSwift.setCtvDivConto("EUR");

        List<SaldoSwift> saldoSwifts = new ArrayList<>();
        saldoSwifts.add(saldoSwift);
        saldoSwifts.add(saldoSwift);
        saldoSwifts.add(saldoSwift);

        when(this.logsResourcePort.addLog(any())).thenReturn(1);
        String test = this.swiftMessageManagerService.saveLog(saldoSwifts);

        when(this.logsResourcePort.addLog(any())).thenReturn(0);
        String test1 = this.swiftMessageManagerService.saveLog(saldoSwifts);

        assertEquals(test,"log scritto correttamente");
        assertEquals(test1,"errore di scrittura  log");
    }

    @Test
    void saveSqlServer() {

        when(this.repositoryResourcePort.saveDb(any())).thenReturn(1);
        int responseTest = this.swiftMessageManagerService.saveSqlServer(any());

        assertEquals(responseTest, "stringTest");
    }
}