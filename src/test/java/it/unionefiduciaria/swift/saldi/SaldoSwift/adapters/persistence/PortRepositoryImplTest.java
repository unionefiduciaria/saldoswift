package it.unionefiduciaria.swift.saldi.SaldoSwift.adapters.persistence;

import it.unionefiduciaria.swift.saldi.SaldoSwift.application.model.SaldoSwift;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.*;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@ExtendWith({SpringExtension.class})
class PortRepositoryImplTest {

    @InjectMocks
    @Spy
    public PortRepositoryImpl portRepositoryImpl;

    @Mock
    private Csv csv;

    @Mock
    private Xlsx xlsx;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }



    @Test
    @ParameterizedTest
    @ValueSource(strings = {"csv","xlsx","none"})
    void factory(String type) throws IOException {

        SaldoSwift saldoSwift = new SaldoSwift();

        saldoSwift.setBic("UBSWCHZH");
        saldoSwift.setMittente("941");
        saldoSwift.setMessaggio("UBSWCHZHX80A");
        saldoSwift.setNomeFlusso("nameSource");
        saldoSwift.setNRifTransazione("Z020336VK7848079");
        saldoSwift.setDepositoSwift("02300000386289600000R");
        saldoSwift.setRapporto("N.D");
        saldoSwift.setDepositario("N.D");
        saldoSwift.setTipoDeposito("N.D");
        saldoSwift.setDepositoSf3("N.D");
        saldoSwift.setDtSaldo("2020/11/30");
        saldoSwift.setSegno("C");
        saldoSwift.setDivisa("EUR");
        saldoSwift.setSaldo("2715295,57");
        saldoSwift.setCambio("0");
        saldoSwift.setCtvDivConto("EUR");

        List<SaldoSwift> saldoSwifts = new ArrayList<>();
        saldoSwifts.add(saldoSwift);

        /*TODO: fixare mocking delle classi che si occupano del salvataggio istanziati nel metodo*/

//        assumingThat(type.equals("none"), () -> assertEquals(noneTest, "none :nessun report\n"));
//        assumingThat(type.equals("csv"), () -> assertEquals(noneTest, "csv :scritto correttamente in " + "testString"+ "\n"));
//        assumingThat(type.equals("xlsx"), () -> assertEquals(noneTest, "xlsx :scritto correttamente in " + "testString"+ "\n"));

    }

    @Test
    void saveDb() {
    }
}